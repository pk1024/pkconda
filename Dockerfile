#-------------------------------------
##	pkconda:Dockerfile
##	Author: Donald Raikes
##	Date:	08/30/2017
#-------------------------------------
FROM	pk1024/pkbase:latest
MAINTAINER	Donald Raikes <dr1861@nyu.edu>

USER	root
##	Make sure everything is up-to-date:
RUN		apt-get update && \
		apt-get upgrade -y 

##	Install and update miniconda2 4.4.0
RUN	curl -LOk https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh  && \
	/bin/bash Miniconda2-latest-Linux-x86_64.sh -b -p /opt/conda2  && \
	echo "export PATH=/opt/conda2/bin:$PATH" > /etc/profile.d/myenv.sh  && \
	rm -f Miniconda2-latest-Linux-x86_64.sh

ENV	PATH=/opt/conda2/bin:$PATH
RUN	conda upgrade --all

